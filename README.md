# Image registration tool

Python package for SIFT based image registration using (mostly) opencv.

### Installation:

1. Clone this repo and `cd` to the project dir.
2. Run `conda env create -f env.yml`. 
3. Run `conda activate image_registration_tool`
4. OPTIONAL: Only if `czi` files need to be read - run `pip install aicspylibczi`
5. Run `pip install .` to install this python package.

### Basic run:

You can run image registration by running i.e. this command:  
`python -m image_registration_tool -d /path/to/input/transformation/destination/image -s /path/to/input/dir/or/a/single/image/to/be/registered -o /path/to/an/output/dir`

### Arguments:  

`-d, --destination_path, required=True, type=str`   
Path to image that the source image/images would be registered to its space.  

`-s, --source_path, required=True, type=str`  
Path to directory with images or to a single image file to be registered to the destination image space.  
If a path to a directory is given and the destination image is in the directory, that file will be skipped as a source and will only be used as destination.  

`-o, --output_path, required=True, type=str`  
Path to output directory. After the analysis the following files will be present (for each source image):
* Transformation matrix (numpy `.npy` file)
* Viewing files to assess the quality of the transformation (only on the channel that was used for the transformation)- registered source image, destination image, overlaid image of registered on destination.
* Registered source image (including all its channels).

`-c, --channel, default=0, type=int`   
The image channel that would be used to calculate the registration - same channel is used for source and destination.  

`-r, --rotate_source_90_clockwise, default=None, type=int, choices=[1, 2, 3]`  
Preprocess step of the source image - rotate it to be roughly at the orientation of the destination image.  
1 - 90 degrees  
2 - 180 degrees  
3 - 270 degrees 
* If input is a directory with multiple images, same preprocessing rotation will be applied to all source images.

`-f, --flip_source, default=None, type=int, choices=[0, 1]`  
Preprocess step of the source image - flip it to be roughly at the orientation of the destination image.  
0 - vertically  
1 - horizontally  
* If input is a directory with multiple images, same preprocessing flipping will be applied to all source images.

`-i, --invert_values_source, action=argparse.BooleanOptionalAction`  
Preprocess step of the source image - invert the grayscale pixel values - dark becomes bright and vice-versa.  
* This is a flag without arguments - if `-i` is a part of the command the pixel values will be inverted.
* If input is a directory with multiple images, same preprocessing pixel value inversion will be applied to all source images.

`-dss, --downscale_source, default=None, type=int, choices=[i for i in range(1, 8)]`
If source image is too big, it can be downscaled to speed up the registration process and match the destination image size.

`-dsd, --downscale_destination, default=None, type=int, choices=[i for i in range(1, 8)]`
If destination image is too big, it can be downscaled to speed up the registration process and match the source image size.

`--n_pyramid_levels, default=0, type=int, choices=[i for i in range(1, 8)]`  
Sets the number of pyramidal downscaling the registered output image will contain. Registered output will be of type `.ome.tif`.  
* This won't take effect if input source is `jpg` or `png`.

`--tile_size, default=5000`    
`--tile_overlap, default=50`   
In case the images are big, the step of finding the SIFT key-points and descriptors is done on tiles of the image.

### Limitations

Current known limitation - registration might fail if rotation between images is large.

### Authors:  
Schmidt Lab of Helmholtz Imaging:
https://helmholtz-imaging.de/   

### License:  

The MIT License  

Copyright (c) 2017-2023 The PyVista Developers  

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.  