from distutils.core import setup

setup(name='image_registration_tool',
version='0.1.0',
description='CV2 and Python image registration tool',
author='Ella Bahry',
author_email='ella.bahry@mdc-berlin.de',
url="https://gitlab.com/ida-mdc/image-registration-tool",
packages=['image_registration_tool'],
)
