from image_registration_tool.registration import run_registration
import argparse
import yaml


def load_config(config_path):
    with open(config_path, 'r') as f:
        return yaml.safe_load(f)


def get_user_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('--config', type=str, help='Path to config YAML file')

    parser.add_argument('-s', '--source_path', help='Path to the source image file.')
    parser.add_argument('-d', '--destination_path', help='Path to the destination image file.')
    parser.add_argument('-o', '--output_path', help='Path where the results will be saved.')

    parser.add_argument('-c', '--channel', default=0, type=int,
                        help='The image channel that would be used to calculate the registration.')
    parser.add_argument('-r', '--rotate_source_90_clockwise', default=None, type=int, choices=[1, 2, 3],
                        help='Number of times the source image should be rotated by 90 degrees clockwise.')
    parser.add_argument('-f', '--flip_source', default=None, type=int, choices=[0, 1],
                        help='Flip the source image horizontally (0) or vertically (1).')
    parser.add_argument('-i', '--invert_values_source', action=argparse.BooleanOptionalAction,
                        help='Invert the pixel values of the source image.')
    parser.add_argument('-dss', '--downsample_source', default=None, type=int,
                        choices=[2, 3, 4, 5, 6, 7, 8], help='Down-sampling factor for the source image.')
    parser.add_argument('-dsd', '--downsample_destination', default=None, type=int,
                        choices=[2, 3, 4, 5, 6, 7, 8], help='Down-sampling factor for the destination image.')
    parser.add_argument('--n_pyramid_levels', default=0, type=int, choices=[i for i in range(1, 8)],
                        help='Sets the number of pyramidal downscaling levels in the registered output image.')
    parser.add_argument('--tile_size', default=5000, type=int,
                        help='Size of the tiles in pixels for SIFT keypoint detection efficiently on large images.')
    parser.add_argument('--tile_overlap', default=50, type=int,
                        help='Overlap size of tiles in pixels for SIFT keypoint detection efficiently on large images.')

    args = parser.parse_args()

    config = {}
    if args.config:
        config = load_config(args.config)

    # Convert args to dict and update with config values
    args_dict = vars(args)
    if config:
        args_dict.update(config)

    # Required parameters check
    required = ['source_path', 'destination_path', 'output_path']
    missing = [param for param in required if not args_dict.get(param)]
    if missing:
        raise ValueError(f"Missing required parameters: {', '.join(missing)}")

    return (
        args_dict['source_path'],
        args_dict['destination_path'],
        args_dict['output_path'],
        args_dict['channel'],
        args_dict['rotate_source_90_clockwise'],
        args_dict['flip_source'],
        args_dict['invert_values_source'],
        args_dict['downsample_source'],
        args_dict['downsample_destination'],
        args_dict['n_pyramid_levels'],
        args_dict['tile_size'],
        args_dict['tile_overlap'],
    )


if __name__ == "__main__":
    source_image_or_dir_path, \
    destination_image_path, \
    output_path, \
    channel_for_sift_analysis, \
    rotate_90_clockwise, \
    flip_image, \
    invert_values, \
    downsample_factor_source, \
    downsample_factor_destination, \
    n_pyramidal_levels, \
    tile_size, \
    tile_overlap = get_user_arguments()

    run_registration(source_image_or_dir_path,
                     destination_image_path,
                     output_path,
                     channel_for_sift_analysis,
                     rotate_90_clockwise,
                     flip_image,
                     invert_values,
                     downsample_factor_source,
                     downsample_factor_destination,
                     n_pyramidal_levels,
                     tile_size,
                     tile_overlap)
