import os
import logging
import sys
from glob import glob
from collections import defaultdict

import cv2
import numpy as np
import tifffile as tif


class RuntimeParameters:
    cv2_warp_max_image_size = 32767
    cv2_seed = 0
    sift = cv2.xfeatures2d.SIFT_create(nfeatures=20000)
    lowes_val = 0.8  # lower is stringent
    ransac_pixel_threshold = 5
    ransac_confidence = 0.95
    rotation_options = {1: cv2.ROTATE_90_CLOCKWISE,
                        2: cv2.ROTATE_180,
                        3: cv2.ROTATE_90_COUNTERCLOCKWISE}
    quality_euclidean_distance_threshold = 3
    max_points_tps = 400

    @classmethod
    def def_flann_matches(cls):
        index_params = dict(algorithm=1, trees=5)
        search_params = dict(checks=50)
        return cv2.FlannBasedMatcher(index_params, search_params)


# def set_logger():
#     logger = logging.getLogger()
#     logger.setLevel(logging.INFO)
#     logging.info("Starting")


def set_logger(output_path):
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Check if handlers are already added to prevent duplicate logs
    if not logger.handlers:
        # Create console handler for output to console
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)

        # Create file handler for output to a file
        os.makedirs(output_path, exist_ok=True)
        file_handler = logging.FileHandler(os.path.join(output_path, 'app.log'))
        file_handler.setLevel(logging.INFO)

        # Define a logging format
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        )
        console_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)

        # Add handlers to the logger
        logger.addHandler(console_handler)
        logger.addHandler(file_handler)

    logger.info("Starting")


def get_list_of_source_images_paths(src_img_or_dir_path, dst_img_path):
    if os.path.isdir(src_img_or_dir_path):
        src_ims_paths = [f for f in glob(os.path.join(src_img_or_dir_path, '*')) if
                         os.path.basename(f) != os.path.basename(dst_img_path)]
    else:
        src_ims_paths = [src_img_or_dir_path]
    logging.info('List of images to register to destination image:\n{}'.format('\n'.join(src_ims_paths)))
    return src_ims_paths


def read_destination_image(dst_img_path, cnl_for_sift_analysis):
    logging.info(f'Reading destination image:')
    image, _ = read_image(dst_img_path, cnl_for_sift_analysis)
    if image is None:
        raise Exception('Destination image path is either not an image or an unsupported format.')
    return image


def read_image(image_path, channel_idx=0):
    logging.info(f'Reading image {image_path}')
    extension = os.path.splitext(image_path)[-1]

    if extension == '.tif':
        image = tif.imread(image_path, key=channel_idx)
    # elif extension == '.czi':
    #     image = read_czi(image_path, channel_idx)
    else:
        image = cv2.imread(image_path)

    if image is None:
        logging.warning(f'{image_path} either not an image file or unsupported format - Skipping')
    else:
        logging.info(f'Read {image_path}, Channel: {channel_idx}, image shape: {image.shape}')
        log_image_metadata(image, is_preprocessed=False)

    return image, extension


# def read_czi(image_path, channel_idx=0):
#     import aicspylibczi
#     czi = aicspylibczi.CziFile(image_path)
#     image = czi.read_mosaic(C=channel_idx)[0]
#     return image


def preprocess_image(image, rotate=None, flip=None, invert=False, is_registration_image=True):

    if is_registration_image:
        if image.ndim == 3:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        image = cv2.normalize(image, None, 0, 255, cv2.NORM_MINMAX).astype('uint8')
        # image = cv2.equalizeHist(image)

        if invert is True:
            image = 255 - image
            logging.info(f'Source image gray scale was inverted.')

    if rotate is not None:
        image = cv2.rotate(image, RuntimeParameters.rotation_options[rotate])
        logging.info(f'Source image was rotated {rotate}*90 clockwise.')
    if flip is not None:
        image = cv2.flip(image, flip)
        logging.info(f'Source image was flipped on axis {flip} (0-vertically, 1-horizontally).')

    log_image_metadata(image)

    return image


def log_image_metadata(image, is_preprocessed=True):
    processed_str = ' after preprocessing' if is_preprocessed else ''
    logging.info(f'image shape{processed_str}: {image.shape}')
    logging.info(f'image data type{processed_str}: {image.dtype}')
    logging.info(f'image range{processed_str}: {np.min(image), np.max(image)}')


def create_output_dir(out_path, dst_img, dst_img_path):
    os.makedirs(out_path, exist_ok=True)
    cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(dst_img_path)}__destination_image.png'), dst_img)


def get_transformation_matrix_with_retry(src_img, dst_img, tile_size_tuple, tile_overlap, out_path=None):
    try:
        return get_sift_based_transformation_matrix(src_img, dst_img, tile_size_tuple, tile_overlap, out_path)
    except AssertionError as e:
        if "Not enough matches" in str(e):
            logging.info("Insufficient matches. Attempting histogram equalization on source image.")
            src_eq = cv2.equalizeHist(src_img)
            try:
                return get_sift_based_transformation_matrix(src_eq, dst_img, tile_size_tuple, tile_overlap, out_path)
            except AssertionError:
                logging.info("Histogram equalization on source still produces insufficient matches. Attempting on destination image.")
                dst_eq = cv2.equalizeHist(dst_img)
                try:
                    return get_sift_based_transformation_matrix(src_img, dst_eq, tile_size_tuple, tile_overlap, out_path)
                except AssertionError:
                    logging.info("Histogram equalization on destination still produces insufficient matches. Attempting on both images.")
                    src_eq = cv2.equalizeHist(src_img)
                    dst_eq = cv2.equalizeHist(dst_img)
                    return get_sift_based_transformation_matrix(src_eq, dst_eq, tile_size_tuple, tile_overlap, out_path)
        else:
            raise


def get_sift_based_transformation_matrix(src_img, dst_img, tile_size_tuple, tile_overlap, out_path=None):

    src_kps, des_kps, matches = get_sift_based_matched_points(src_img, dst_img, tile_overlap, tile_size_tuple)

    src_pts_np, des_pts_np = get_points_as_np(matches, src_kps, des_kps)

    h, msk = cv2.findHomography(src_pts_np, des_pts_np, cv2.RANSAC,
                                RuntimeParameters.ransac_pixel_threshold,
                                RuntimeParameters.ransac_confidence)
    logging.info('DONE: calculated homography matrix')
    output_n_inliers(msk)

    save_kps_matches_images(src_img, dst_img, src_kps, des_kps, matches, out_path, is_ransac=False)
    matches = filter_matches(matches, msk)
    save_kps_matches_images(src_img, dst_img, src_kps, des_kps, matches, out_path, is_ransac=True)

    src_pts_np, des_pts_np = get_points_as_np(matches, src_kps, des_kps)

    return h, src_pts_np, des_pts_np


def filter_matches(matches, msk):
    msk = msk.ravel().astype(bool)
    filtered_matches = [match for match, keep in zip(matches, msk) if keep]
    return filtered_matches


def get_sift_based_matched_points(src_img, dst_img, tile_overlap, tile_size_tuple):
    src_kps, src_desc = compute_sift_tiles(src_img, tile_size_tuple, tile_overlap, RuntimeParameters.sift)
    des_kps, des_desc = compute_sift_tiles(dst_img, tile_size_tuple, tile_overlap, RuntimeParameters.sift)

    logging.info('DONE: SIFT-based keypoint detection and descriptor computation')

    check_kps_within_image(src_kps, src_img.shape)

    src_kps, src_desc = filter_duplicate_kps(src_kps, src_desc)
    des_kps, des_desc = filter_duplicate_kps(des_kps, des_desc)

    src_kps, src_desc = keep_top_keypoints(src_kps, src_desc)
    des_kps, des_desc = keep_top_keypoints(des_kps, des_desc)

    # matches12, matches21 = get_bf_matches(src_desc, des_desc)
    matches12, matches21 = get_flann_matches(src_desc, des_desc)

    good_matches = get_good_mutual_matches(matches12, matches21)
    # good_matches = get_good_matches(matches12)

    logging.info('DONE: obtained good matches by distance thresholding')

    return src_kps, des_kps, good_matches


def check_kps_within_image(kps, img_shape):
    height, width = img_shape
    out_of_bounds = []
    out_of_bounds_indices = []

    for i, kp in enumerate(kps):
        x, y = kp.pt
        if not (0 <= x < width and 0 <= y < height):
            out_of_bounds.append((x, y))
            out_of_bounds_indices.append(i)

    if out_of_bounds:
        logging.error(f"{len(out_of_bounds)} key-points are outside the image boundaries: {out_of_bounds}")


def filter_duplicate_kps(kps, desc):
    point_to_indices = defaultdict(list)
    for idx, kp in enumerate(kps):
        point_to_indices[tuple(kp.pt)].append(idx)

    unique_indices = []
    for indices in point_to_indices.values():
        if len(indices) > 1:
            best_idx = max(indices, key=lambda i: kps[i].response)
            unique_indices.append(best_idx)
        else:
            unique_indices.append(indices[0])

    unique_kps = [kps[i] for i in unique_indices]
    unique_desc = desc[unique_indices]

    return unique_kps, unique_desc


def keep_top_keypoints(kp, des, n_top=100000):
    kp_des = list(zip(kp, des))
    kp_des_sorted = sorted(kp_des, key=lambda x: x[0].response, reverse=True)
    kp_des_best = kp_des_sorted[:n_top]
    kp_best, des_best = zip(*kp_des_best)
    return list(kp_best), np.array(des_best)

def get_flann_matches(des1, des2):
    matches12 = RuntimeParameters.def_flann_matches().knnMatch(des1, des2, k=2)
    matches21 = RuntimeParameters.def_flann_matches().knnMatch(des2, des1, k=2)
    return matches12, matches21


def get_bf_matches(des1, des2):
    bf = cv2.BFMatcher(cv2.NORM_L2)
    matches12 = bf.knnMatch(des1, des2, k=2)
    matches21 = bf.knnMatch(des2, des1, k=2)
    return matches12, matches21


def get_points_as_np(matches, src_kps, des_kps):
    if len(matches) >= 6:
        src_pts_np = np.float32([src_kps[m.queryIdx].pt for m in matches]).reshape(-1, 1, 2)
        des_pts_np = np.float32([des_kps[m.trainIdx].pt for m in matches]).reshape(-1, 1, 2)
    else:
        raise AssertionError("Not enough matches found for image registration.")
    return src_pts_np, des_pts_np


def get_good_matches(matches12):
    good_matches = []
    for m, n in matches12:
        if m.distance < RuntimeParameters.lowes_val * n.distance:
            good_matches.append(m)

    logging.info(f'Number of good Lowes ratio test matches: {len(good_matches)}')

    return good_matches


def get_good_mutual_matches(matches12, matches21):

    good_matches12 = [m for m, n in matches12 if m.distance < RuntimeParameters.lowes_val * n.distance]
    good_matches21 = [m for m, n in matches21 if m.distance < RuntimeParameters.lowes_val * n.distance]

    matches12_dict = {(m.queryIdx, m.trainIdx): m for m in good_matches12}
    matches21_dict = {(m.trainIdx, m.queryIdx): m for m in good_matches21}

    mutual_matches = []
    for (query_idx1, train_idx1), m12 in matches12_dict.items():
        m21 = matches21_dict.get((query_idx1, train_idx1), None)
        if m21 is not None:
            distance = (m12.distance + m21.distance) / 2.0
            match = cv2.DMatch(_queryIdx=query_idx1, _trainIdx=train_idx1, _distance=distance)
            mutual_matches.append(match)

    logging.info(f'Number of mutual matches: {len(mutual_matches)}')
    return mutual_matches


def compute_sift_tiles(image, tile_size_tuple, tile_overlap, sift):

    height, width = image.shape[:2]
    tile_width, tile_height = tile_size_tuple

    if width <= tile_width or height <= tile_height:
        keypoints, descriptors = sift.detectAndCompute(image, None)
        return keypoints, np.array(descriptors)

    tile_overlap = adjust_tile_overlap(tile_overlap, tile_width, tile_height)

    step_x = tile_width - tile_overlap
    step_y = tile_height - tile_overlap

    x_starts = list(np.arange(0, width - tile_overlap, step_x))
    y_starts = list(np.arange(0, height - tile_overlap, step_y))

    keypoints = []
    descriptors = []

    for y in y_starts:
        for x in x_starts:
            tile = image[y:min(y + tile_height, height), x:min(x + tile_width, width)]
            tile_keypoints, tile_descriptors = sift.detectAndCompute(tile, None)

            # Adjust keypoint coordinates
            for kp in tile_keypoints:
                adjusted_kp = cv2.KeyPoint(
                    x=kp.pt[0] + x,
                    y=kp.pt[1] + y,
                    size=kp.size,
                    angle=kp.angle,
                    response=kp.response,
                    octave=kp.octave,
                    class_id=kp.class_id
                )
                keypoints.append(adjusted_kp)

            if tile_descriptors is not None and len(tile_descriptors) > 0:
                descriptors.extend(tile_descriptors)

    return keypoints, np.array(descriptors)


def adjust_tile_overlap(tile_overlap, tile_width, tile_height):
    max_overlap = 100
    half_tile_width = tile_width // 2
    half_tile_height = tile_height // 2
    allowed_overlap = min(max_overlap, half_tile_width, half_tile_height)

    if tile_overlap > allowed_overlap:
        logging.warning(
            f"Tile overlap {tile_overlap} exceeds the allowed maximum of {allowed_overlap}. "
            f"Setting tile_overlap to {allowed_overlap}."
        )
        tile_overlap = allowed_overlap
    else:
        logging.info(f"Using tile_overlap: {tile_overlap}")
    return tile_overlap


def save_h(out_path, h, src_img_path):
    np.save(os.path.join(out_path, f'{os.path.basename(src_img_path)}__registration_matrix.npy'), h)


def save_kps_matches_images(src_img, des_img, src_kps, des_kps, matches, out_path, is_ransac):
    matches_img = cv2.drawMatches(src_img, src_kps, des_img, des_kps,
                                  matches, None, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    cv2.imwrite(os.path.join(out_path, f'forViewing_all_matches_ransac{is_ransac}.png'), matches_img)
    logging.info(f'Saved matches viewing image. Number of matches RANSAC {is_ransac}: {len(matches)}')


def warp_image_homography(dst_img_shape, src_img, h):
    if max(src_img.shape) < RuntimeParameters.cv2_warp_max_image_size \
            and max(dst_img_shape) < RuntimeParameters.cv2_warp_max_image_size:
        registered_img = cv2.warpPerspective(src_img, h, dst_img_shape[::-1])
    else:
        registered_img = warp_homography_using_skimage(dst_img_shape, src_img, h)

    logging.info('Registered image')

    return registered_img


def warp_homography_using_skimage(dst_img_shape, src_img, h):
    try:
        import skimage.transform
    except ImportError:
        logging.warning(
            "Images width/height is larger than 32767 - Please install skimage and rerun -" +
            " due to cv2 warp function limitation.")
        raise AssertionError("Not enough matches found for image registration.")

    h_inv = np.linalg.inv(h)
    registered_img = skimage.transform.warp(src_img, h_inv, output_shape=dst_img_shape,
                                            preserve_range=True).astype(src_img.dtype)

    return registered_img


def check_transformation_metrics(src_pts, dst_pts, h, src_img, dst_img, registered_img):
    if check_degenerate(src_pts) or check_degenerate(dst_pts):
        logging.warning("Degenerate keypoint configuration detected! Might result in a faulty homography matrix!!")
        return False

    if not check_transformation_by_pts_mean_sqrt(src_pts, dst_pts, h):
        return False

    if not check_transformation_by_point_distribution(h, src_img):
        return False

    check_transformation_by_joint_entropy(registered_img, dst_img)
    check_transformation_by_mse(registered_img, dst_img)

    return True


def check_degenerate(points):
    std_dev = np.std(points, axis=0)

    # If the standard deviation is below a threshold (here 1.0),
    # it might indicate that the points are in a degenerate configuration.
    if np.any(std_dev < 1.0):
        logging.warning("Points may be in a degenerate configuration.")


def check_transformation_by_pts_mean_sqrt(src_pts, dst_pts, h):
    mapped_src_pts = cv2.perspectiveTransform(src_pts, h)
    distances = np.sqrt(np.sum((mapped_src_pts - dst_pts) ** 2, axis=2))
    mean_distance = np.mean(distances)

    logging.info(f'Mean Euclidean distance of mapped source key-points and destination key-points: {mean_distance}')

    if mean_distance > RuntimeParameters.quality_euclidean_distance_threshold:
        logging.warning(
            "Transformation may be poor - mean euclidean distance of mapped source and destination key points is high!")
        return False

    return True


def output_n_inliers(msk):
    msk = msk.ravel().tolist()

    n_inliers = msk.count(1)
    n_outliers = msk.count(0)

    logging.info(f'RANSAC number of Inliers: {n_inliers}')
    logging.info(f'RANSAC number of Outliers: {n_outliers}')


def check_transformation_by_point_distribution(h, src_img):
    height, width = src_img.shape
    x = np.linspace(0, width, num=50)
    y = np.linspace(0, height, num=50)
    xv, yv = np.meshgrid(x, y)
    points = np.column_stack([xv.ravel(), yv.ravel()])

    points_t = cv2.perspectiveTransform(points.reshape(-1, 1, 2), h)

    std_dev = np.std(points_t, axis=0)

    if np.any(std_dev < 1.0) or np.any(std_dev > max(height, width)):
        logging.warning("Transformation may be poor - transformed points grid seem to be concentrated!")
        return False

    return True


def check_transformation_by_joint_entropy(image1, image2, bins=256):
    try:
        from scipy.stats import entropy
    except ImportError:
        logging.warning("Joint entropy won't be used as a metric to check registration as scipy is not installed.")
        return

    if 'scipy' in sys.modules:
        hist_2d, _, _ = np.histogram2d(image1.ravel(), image2.ravel(), bins=bins)
        joint_prob = hist_2d / np.sum(hist_2d)

        logging.info(f'Joint entropy (lower is better): {entropy(joint_prob.ravel())}')


def check_transformation_by_mse(image1, image2):
    err = np.sum((image1.astype("float") - image2.astype("float")) ** 2)
    err /= float(image1.shape[0] * image1.shape[1])

    logging.info(f'Mean square error (lower is better): {err}')


def get_overlay_image(dst_img, registered_img):
    dst_img_rgb = cv2.cvtColor(dst_img, cv2.COLOR_GRAY2RGB)
    registered_img_colormap = cv2.applyColorMap(registered_img, cv2.COLORMAP_HOT)

    overlaid_img = cv2.addWeighted(dst_img_rgb, 0.7, registered_img_colormap, 0.3, 0)

    return overlaid_img


def save_analysis_images(out_path, registered_perspective_img, registered_tps_img, src_img, src_img_path, overlaid_img):
    cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(src_img_path)}__registered_perspective.png'),
                registered_perspective_img)
    if registered_tps_img is not None:
        cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(src_img_path)}__registered_tps.png'),
                    registered_tps_img)
    cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(src_img_path)}__source.png'), src_img)
    cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(src_img_path)}__overlaid.png'), overlaid_img)
    if overlaid_img.size >= 178956970:
        cv2.imwrite(os.path.join(out_path, f'forViewing_{os.path.basename(src_img_path)}__overlaid0.5.png'),
                    cv2.pyrDown(overlaid_img))
    logging.info('Saved the images that were used for registration')


def transform_original_image_and_save(out_path, src_img_path, h, dst_img_shape, src_extension,
                                      rotate, flip, src_d_type, n_pyramid_levels):

    n_channels = get_n_channels(src_img_path, src_extension)

    if src_extension.lower() in [".jpeg", ".jpg", ".png"]:
        read_register_and_save_png(dst_img_shape, h, out_path, src_img_path, rotate, flip)
    else:
        read_register_and_save_tif(dst_img_shape, h, n_channels, out_path, src_img_path,
                                   rotate, flip, src_d_type, n_pyramid_levels)

    logging.info('Saved transformed original image successfully.')


def read_register_and_save_png(dst_img_shape, h, out_path, src_img_path, rotate, flip):
    image = cv2.imread(src_img_path)
    image = preprocess_image(image, rotate, flip, False, False)
    registered_img = warp_image_homography(dst_img_shape, image, h)
    cv2.imwrite(os.path.join(out_path, f'{os.path.basename(src_img_path)}_registered.png'), registered_img)


def channel_generator(image_path, dst_img_shape, h, rotate, flip):
    with tif.TiffFile(image_path) as tif_file:
        for channel_idx in range(len(tif_file.pages)):
            src_img = tif_file.pages[channel_idx].asarray()
            src_img = preprocess_image(src_img, rotate, flip, False, False)
            registered_img = warp_image_homography(dst_img_shape, src_img, h)
            yield registered_img


def image_resizing_generator(image_path, level_shape, dst_img_shape, h, rotate, flip):
    for img in channel_generator(image_path, dst_img_shape, h, rotate, flip):
        resized_img = cv2.resize(img, level_shape, interpolation=cv2.INTER_AREA)
        yield resized_img


def read_register_and_save_tif(dst_img_shape, h, n_channels, out_path, src_img_path,
                               rotate, flip, src_d_type, n_pyramid_levels=2):

    # is_ome = True if 'ome.tif' in src_img_path.lower() else False
    is_ome = True if n_pyramid_levels else False
    extension = '.ome.tif' if is_ome else '.tif'

    logging.info(src_d_type)

    metadata = {'axes': 'CYX' if n_channels > 1 else 'YX',
                'SignificantBits': 8,
                'Channel': {'Name': [f'Channel {c}' for c in range(n_channels)]}
                }

    file_path = os.path.join(out_path, f'{os.path.basename(src_img_path)[:-4]}_registered{extension}')

    with tif.TiffWriter(file_path, bigtiff=True, ome=is_ome) as t:

        t.write(channel_generator(src_img_path, dst_img_shape, h, rotate, flip),
                subifds=n_pyramid_levels, metadata=metadata, shape=(n_channels, *dst_img_shape), dtype=src_d_type)

        logging.info('Original image saved as .tif')

        for level in range(n_pyramid_levels):
            level_shape = [s // 2 ** (level + 1) for s in dst_img_shape]
            t.write(image_resizing_generator(src_img_path, level_shape[::-1], dst_img_shape, h, rotate, flip),
                    subfiletype=1, shape=(n_channels, *level_shape), dtype=src_d_type)
            logging.info(f'Downscaled registered image was saved to .ome.tif pyramidal file - level {level}')


def get_n_channels(image_path, extension):
    if extension == '.tif':
        tif_obj = tif.TiffFile(image_path)
        n_channels = len(tif_obj.pages)
    # elif extension == '.czi':
    #     czi = read_czi(image_path)
    #     n_channels = czi.get_dims_shape()[0]['C'][1]
    else:
        n_channels = 1

    return n_channels


def downscale_image(image, downsample_factor):

    if downsample_factor is None:
        return image
    elif downsample_factor <= 1:
        logging.warning('down-sample factor should be greater than 1 - skipping down-sample.')
        return image

    new_height = image.shape[0] // downsample_factor
    new_width = image.shape[1] // downsample_factor

    sigma = downsample_factor / 2
    blurred_image = cv2.GaussianBlur(image, (0, 0), sigmaX=sigma, sigmaY=sigma)
    downscaled_image = cv2.resize(blurred_image, (new_width, new_height), interpolation=cv2.INTER_AREA)

    return downscaled_image


def adjust_homography(h, source_factor, dest_factor):

    if source_factor is not None:
        source_scale = 1/source_factor
        s_down = np.array([[source_scale, 0, 0],
                           [0, source_scale, 0],
                           [0, 0, 1]])
        s_up = np.array([[1, 0, 0],
                         [0, 1, 0],
                         [0, 0, 1]])
        h = np.dot(np.dot(s_up, h), s_down)

    if dest_factor is not None:
        s_down = np.array([[1, 0, 0],
                           [0, 1, 0],
                           [0, 0, 1]])
        s_up = np.array([[dest_factor, 0, 0],  # Correcting back to the original scale
                         [0, dest_factor, 0],
                         [0, 0, 1]])

        h = np.dot(np.dot(s_up, h), np.linalg.inv(s_down))

    return h


def select_matching_points_from_grid(points1, points2, img_shape, num_points_desired):
    aspect_ratio = img_shape[1] / img_shape[0]
    grid_rows = int(np.sqrt(num_points_desired / aspect_ratio))
    grid_cols = int(num_points_desired / grid_rows)

    grid_rows = max(10, grid_rows)
    grid_cols = max(10, grid_cols)

    cell_height = img_shape[0] // grid_rows
    cell_width = img_shape[1] // grid_cols
    selected_points1 = []
    selected_points2 = []

    for i in range(grid_rows):
        for j in range(grid_cols):
            cell_indices = [
                idx for idx, (x, y) in enumerate(points1[:, 0, :])
                if i * cell_height <= y < (i + 1) * cell_height
                and j * cell_width <= x < (j + 1) * cell_width
            ]

            if cell_indices:
                selected_idx = np.random.choice(cell_indices)
                selected_points1.append(points1[selected_idx])
                selected_points2.append(points2[selected_idx])

    return selected_points1, selected_points2


def get_tps_transformed_img(source_points, destination_points, src_img, h):

    if len(source_points) > RuntimeParameters.max_points_tps:
        source_points, destination_points = select_matching_points_from_grid(source_points,
                                                                             destination_points,
                                                                             src_img.shape,
                                                                             RuntimeParameters.max_points_tps)

        logging.info(f'Number of points for TPS transformation: {len(source_points)}')

    source_points = np.asarray(source_points, dtype=np.float32)
    # source_points = source_points.reshape(-1, 1, 2)
    destination_points = np.asarray(destination_points, dtype=np.float32)
    # destination_points = destination_points.reshape(-1, 1, 2)

    source_points = cv2.perspectiveTransform(source_points, h)

    tps = cv2.createThinPlateSplineShapeTransformer()
    matches = [cv2.DMatch(i, i, 0) for i in range(len(source_points))]

    tps.estimateTransformation(np.array(destination_points).reshape((-1, len(source_points), 2)),
                               np.array(source_points).reshape((-1, len(source_points), 2)),
                               matches)
    output_image = tps.warpImage(src_img)

    return output_image


def run_registration(source_image_or_dir_path,
                     destination_image_path,
                     output_path,
                     channel_for_sift_analysis,
                     rotate_90_clockwise,
                     flip_image,
                     invert_values,
                     downsample_factor_source,
                     downsample_factor_destination,
                     n_pyramidal_levels,
                     tile_size,
                     tile_overlap):

    set_logger(output_path)

    destination_image = read_destination_image(destination_image_path, channel_for_sift_analysis)
    destination_image = preprocess_image(destination_image, None, None, False, True)
    destination_image_ds = downscale_image(destination_image, downsample_factor_destination)
    create_output_dir(output_path, destination_image, destination_image_path)

    source_images_paths = get_list_of_source_images_paths(source_image_or_dir_path, destination_image_path)
    for source_image_path in source_images_paths:
        cv2.setRNGSeed(RuntimeParameters.cv2_seed)
        source_image, source_extension = read_image(source_image_path, channel_for_sift_analysis)
        if source_image is None:
            logging.warning(f'Skipping {source_image_path} due to read error.')
            continue

        source_image = downscale_image(source_image, downsample_factor_source)
        source_image = preprocess_image(source_image, rotate_90_clockwise, flip_image, invert_values, True)

        h, src_pts, des_pts = get_transformation_matrix_with_retry(source_image,
                                                                   destination_image_ds,
                                                                   (tile_size, tile_size),
                                                                   tile_overlap,
                                                                   output_path)

        registered_image_h = warp_image_homography(destination_image_ds.shape, source_image, h)

        if downsample_factor_source is not None or downsample_factor_destination is not None:
            h = adjust_homography(h, downsample_factor_source, downsample_factor_destination)

        save_h(output_path, h, source_image_path)

        is_good = check_transformation_metrics(src_pts,
                                               des_pts,
                                               h,
                                               source_image,
                                               destination_image_ds,
                                               registered_image_h)

        if is_good:
            registered_image_tps = get_tps_transformed_img(src_pts, des_pts, registered_image_h, h)
        else:
            logging.warning('Thin Plate Spline transformation was not applied due to poor homography matrix.')
            registered_image_tps = None

        overlaid_image = get_overlay_image(destination_image_ds, registered_image_h)
        save_analysis_images(output_path, registered_image_h, registered_image_tps,
                             source_image, source_image_path, overlaid_image)

        source_d_type = source_image.dtype
        del registered_image_h, source_image, overlaid_image

        transform_original_image_and_save(output_path, source_image_path, h, destination_image.shape,
                                          source_extension, rotate_90_clockwise, flip_image,
                                          source_d_type, n_pyramidal_levels)

        if not is_good:
            logging.warning('Transformation seems to be faulty!')
            logging.warning('If source and destination image differ too much, '
                            'please rerun with preprocessing parameters for image rotation, flip, and inversion.')
